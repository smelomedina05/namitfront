export const IMAGS = {
  "movie1" : {
    screen:"https://m.media-amazon.com/images/M/MV5BMzUzNDM2NzM2MV5BMl5BanBnXkFtZTgwNTM3NTg4OTE@._V1_UX182_CR0,0,182,268_AL_.jpg",
    art:"http://t1.gstatic.com/images?q=tbn:ANd9GcRTeGqgCfnw0J5becKNZ46EqVmeGOxTvjQfEl3LjlKY1AOgLcqv"
  },
  "movie2": {
    screen:"https://m.media-amazon.com/images/M/MV5BOTA5NDZlZGUtMjAxOS00YTRkLTkwYmMtYWQ0NWEwZDZiNjEzXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg",
    art: "http://stc.obolog.net/photos/565a/565a4a45d4ff2s86843_p.jpg"
  },
  "movie3": {
    screen:"https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    art:"https://images-na.ssl-images-amazon.com/images/I/51k98elC6mL.jpg"
  },
  "movie4": {
    screen:"https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    art:"https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/joker-poster-2-1567070106.jpg?crop=1xw:1xh;center,top&resize=980:*"
  },
  "movie5": {
    screen:"https://m.media-amazon.com/images/M/MV5BNzY2NzI4OTE5MF5BMl5BanBnXkFtZTcwMjMyNDY4Mw@@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
    art:"https://static.posters.cz/image/1300/poster/black-swan-cisne-negro-natalie-portman-i16318.jpg"
  },
  "movie6": {
    screen:"https://m.media-amazon.com/images/M/MV5BMTg2MjkwMTM0NF5BMl5BanBnXkFtZTcwMzc4NDg2NQ@@._V1_SY1000_SX675_AL_.jpg",
    art:"https://images-na.ssl-images-amazon.com/images/I/71kzh03v6sL._SY445_.jpg"
  },
  "movie7": {
    screen:"https://d2e111jq13me73.cloudfront.net/sites/default/files/styles/product_image_aspect_switcher_170w/public/product-images/csm-movie/friends-with-benefits-poster.jpg?itok=Wus5maHq",
    art:"https://occ-0-2954-2567.1.nflxso.net/dnm/api/v6/XsrytRUxks8BtTRf9HNlZkW2tvY/AAAABXp51KAEh_jmGtMeymclty8OiWYcTwf-hMqyu_PCIosVnap6yi62JBEDYGaraYv-YMvBATnR-Sy18eQJ7D0NVoRagJwKeP2PzA.jpg?r\x3D3d9"
  },
  "movie8": {
    screen:"https://3.bp.blogspot.com/-JYmG3yws_CU/VCROTO9hsyI/AAAAAAAAeDw/lzTRpB25Wkg/s1600/bradley%2Bcooper%2Bbipolar.jpg",
    art:"https://3.bp.blogspot.com/-JYmG3yws_CU/VCROTO9hsyI/AAAAAAAAeDw/lzTRpB25Wkg/s1600/bradley%2Bcooper%2Bbipolar.jpg"
  },
  "movie9": {
    screen:"https://i.blogs.es/8d2c30/watchmen-imax/1366_2000.jpg",
    art:"https://render.fineartamerica.com/images/rendered/default/poster/8/10/break/images/artworkimages/medium/1/no599-my-watchmen-minimal-movie-poster-chungkong-art.jpg"
  },
  "movie10": {
    screen:"https://mypostercollection.com/wp-content/uploads/2018/07/Fight-Club-MyPosterCollection.com-1-683x1024.jpg",
    art:"https://cdn.shopify.com/s/files/1/1416/8662/products/fight_club_1999_french_original_film_art_61d76239-16d1-47a9-9836-ed1569c22a31_2000x.jpg?v=1564770755"
  },
  "movie11": {
    screen:"https://cdn.shopify.com/s/files/1/1416/8662/products/inception_2010_advance_original_film_art_dc7d7689-1e66-41d6-b228-48da4e7b52e4_2000x.jpg?v=1564717627",
    art:"https://www.arthipo.com/image/cache/catalog/genel-tasarim/all-posters/sinema-cinema-film-postersleri/yabanci-filmler/pfilm127-inception-poster-baslangic-1000x1000.jpg"
  },
  "movie12": {
    screen:"",
    art:""
  },
};
