
export const MOVIEDETAILS = {
  "movie1": {
    id: "movie1",
    name:"La la land", description:"Aspiring actress serves lattes to movie stars in between auditions and jazz musician Sebastian scrapes by playing cocktail-party gigs in dingy bars. But as success mounts, they are faced with decisions that fray the fragile fabric of their love affair, and the dreams they worked so hard to maintain in each other threaten to rip them apart.",
    rate:4.8, languages:["English", "Spanish"],
    related: ["movie5", "movie8", "movie2", "movie6", "movie7"],
    casting: ["Emma Stone", "Ryan Gosling"]
  },
  "movie3": {
    id: "movie3",
    name:"Batman, The Dark Knight", description:"One year after the events of Batman Begins (2008), Batman, Lieutenant James Gordon, and District Attorney Harvey Dent plan to launch an attack on the mob by arresting the shady accountant of the mob, Lau. Lau is abducted from his building by Batman and is thrown into jail. Lau divulges the secrets which results in almost all the mob bosses thrown in jail. The desperate mob bosses turn to The Joker, a sadistic psychopath with green hair, completely yellow teeth, and a custom purple suit. The Joker kills a judge, the Police Commissioner, and tries to kill the Mayor and Harvey. The acts of The Joker produces anarchy and chaos in the people of Gotham, forcing Batman to come to terms to which may seem to be his greatest test to fight injustice and come closer to the fine line between hero and vigilante.",
    rate:4.7, languages:["English", "Spanish"],
    related: ["movie9", "movie11", "movie10", "movie4"],
    casting: ["Heath Ledger"]
  },
  "movie2": {
    id: "movie2",
    name:"Whiplash", description:"Andrew Neiman is an ambitious young jazz drummer, single-minded in his pursuit to rise to the top of his elite east coast music conservatory. Plagued by the failed writing career of his father, Andrew hungers day and night to become one of the greats. Terence Fletcher, an instructor equally known for his teaching talents as for his terrifying methods, leads the top jazz ensemble in the school. Fletcher discovers Andrew and transfers the aspiring drummer into his band, forever changing the young man's life. Andrew's passion to achieve perfection quickly spirals into obsession, as his ruthless teacher continues to push him to the brink of both his ability-and his sanity.",
    rate:4.6, languages:["English", "Spanish"],
    related: ["movie7", "movie1", "movie5", "movie6", "movie8"],
    casting: ["J.K Simmons"]
  },
  "movie4": {
    id: "movie4",
    name:"Joker", description:"Arthur Fleck is a wannabe stand-up comic who suffers from many mental illnesses, including one which causes him to laugh uncontrollably when he is nervous, and often gets him into bad situations. Arthur's mental health causes almost all people in society to reject and look down upon him, even though all he wants is to be accepted by others. After being brutally beaten, having his medication cut off, Arthur's life begins to spiral downward out-of-control into delusions, violence, and anarchy until he eventually transforms into Gotham's infamous Clown-Prince of Crime.",
    rate:4.4, languages:["English", "Spanish"],
    related: ["movie11", "movie10", "movie3", "movie9"],
    casting: ["Joaquin Phoenix"]
  },
  "movie5": {
    id: "movie5",
    name:"Black Swan", description:"Nina Sayers is a dancers with a New York City ballet company. She is dedicated to her art and her craft but lacks confidence. She is also pressed on by her domineering mother, also a ballerina in her youth, who gave up dancing when she got pregnant with Nina. Nina seems to achieve her goal when the company's artistic director Thomas Leroy selects her as the lead in their new production of Swan Lake. Leroy is honest however in telling her that while he has no doubt she can handle the role of the white swan, he's not sure she can handle the role of the black swan. Adding to her self-doubts is the arrival of a new dancer, Lily, who seems ideally suited to playing the black swan. To succeed, Nina will will have to transform herself into something she has never been.",
    rate:4.2, languages:["English", "Spanish"],
    related: ["movie1", "movie2", "movie8", "movie7", "movie6"],
    casting: ["Natalie Portman"]
  },
  "movie6": {
    id: "movie6",
    name:"Crazy, Stupid, Love.", description:"Cal and Emily have been married for over 20 years. But when Cal learns that Emily had an affair he moves out. He would then go to a bar and whine about what happened. Jacob, a regular at the bar, upon hearing his woes offers to help him. By giving him a makeover and teaching him how to be a player. It isn't long before he's picking up someone frequently. But eventually he realizes he still loves Emily and wishes he could go back. At the same time his son is nursing a crush on his baby sitter who doesn't feel the same way. And even Jacob gets a new outlook when he starts to feel something about a girl he tried to pick up earlier, but she turned him down because she has a boyfriend.",
    rate:3.8, languages:["English", "Spanish"],
    related: ["movie2", "movie5", "movie1", "movie7"],
    casting: ["Emma Stone", "Ryan Gosling"]
  },
  "movie7": {
    id: "movie7",
    name:"Friends with Benefits", description:"Dylan realizes how he feels about Jamie and after a talk with his friend and coworker, Tommy (Woody Harrelson), decides to go after her. He calls Jamies mother to set up an excuse to get Jamie to go to Grand Central Station thinking she will be picking her mother up and arranges to have another flash mob scene set up to surprise Jamie at Grand Central. When the moment comes he catches up with Jamie and tells her how he really feels. Surprised and happy by this turn of events Jamie tells him to kiss her. After sharing a kiss Dylan suggests it is time they go on their first real date. They go to the café across the street, and although they attempt to keep the date casual and relaxed, the film ends with them in a sensual embrace and passionate kiss.",
    rate:4.5, languages:["English", "Spanish"],
    related: ["movie2", "movie5", "movie1", "movie8", "movie6"],
    casting: ["Justin Timberlake", "Mila Kunis"]
  },
  "movie8": {
    id: "movie8",
    name:"Silver Linings Playbook", description:"Pat Solitano upon discovering his wife with another man beats him. He would be sent to a mental hospital where he would be diagnosed as suffering from bipolar disorder. He would get released and would go live with his parents. But he is reluctant to take his meds because he doesn't like how they make him feel or go to mandated therapy. The only thing he wants is to try and get back together with his wife but she already wrote him off and got a restraining order on him. He goes around trying to find someone who might know where she is but doesn't. His friend, Ronnie invites him to dinner and he meets Ronnie's sister-in-law, Tiffany who's also reeling from a personal loss. They make a connection but Pat is devoted to his wife refuses to do anything. When he learns she sees his wife, he asks her to give his wife a letter. She agrees on the condition that he be her dance partner at upcoming competition. He agrees and they establish a rapport.",
    rate:4.4, languages:["English", "Spanish"],
    related: ["movie2", "movie5", "movie1", "movie6", "movie7"],
    casting: ["Bradley Cooper", "Jennifer Lawrence "]
  },
  "movie9": {
    id: "movie9",
    name:"Watchmen", description:"In a gritty and alternate 1985 the glory days of costumed vigilantes have been brought to a close by a government crackdown, but after one of the masked veterans is brutally murdered an investigation into the killer is initiated. The reunited heroes set out to prevent their own destruction, but in doing so discover a deeper and far more diabolical plot.",
    rate:4.0, languages:["English", "Spanish"],
    related: ["movie3", "movie4", "movie10", "movie11"],
    casting: ["Malin Akerman", " Billy Crudup", "Matthew Goode", "Jeffrey Dean Morgan"]
  },
  "movie10": {
    id: "movie10",
    name:"Fight Club", description:"A nameless first person narrator attends support groups in attempt to subdue his emotional state and relieve his insomniac state. When he meets Marla, another fake attendee of support groups, his life seems to become a little more bearable. However when he associates himself with Tyler he is dragged into an underground fight club and soap making scheme. Together the two men spiral out of control and engage in competitive rivalry for love and power. When the narrator is exposed to the hidden agenda of Tyler's fight club, he must accept the awful truth that Tyler may not be who he says he is.",
    rate:4.3, languages:["English", "Spanish"],
    related: ["movie3", "movie4", "movie9", "movie11"],
    casting: ["Edward Norton", "Brad Pitt"]
  },
  "movie11": {
    id: "movie11",
    name:"Inception", description:"Dom Cobb is a thief with the rare ability to enter people's dreams and steal their secrets from their subconsciouses. His skill has made him a hot commodity in the world of corporate espionage, but has also cost him everything he loves. Cobb gets a chance at redemption when he is offered a seemingly impossible task: Plant an idea in someone's mind. If he succeeds, it will be the perfect crime, but a dangerous enemy anticipates Cobb's every move.",
    rate:4.1, languages:["English", "Spanish"],
    related: ["movie3", "movie4", "movie9", "movie10"],
    casting: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"]
  },

};
